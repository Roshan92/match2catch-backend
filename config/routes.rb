Rails.application.routes.draw do

  resources :sms, only: [:new, :create]

  #devise_for :users

  devise_for :users, :path=>"", :path_names => {:sign_in => 'login', :sign_out => 'logout', :sign_up => 'register'}

  resources :users, except: :create
  # get "/users" => 'users#index'
  # get 'user/:id' => 'users#show', :as => 'user'
  # get 'users/new' => 'users#new'
  # get "/about" => 'welcome#about'
  # get "/contributors" => 'welcome#contributors'
  # get "/contact" => 'welcome#contact'
  # get "/features" => 'welcome#features'

  #post "/relationships" => 'relationships#create'
  #delete "/relationships" => 'relationships#destroy'
  resources :relationships, :only => [:create, :destroy]

  get "dashboard" => 'dashboard#index'
  namespace :dashboard do
  	get "profile"
	  put "update"
    get "users"
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"

  devise_scope :user do
    root to: "devise/sessions#new"
  end

    shallow do
      resources :leagues do
        resources :divisions do
          resources :teams
        end
        resources :locations
        resources :games
        resources :standings
      end

      resources :conversations do
        resources :messages
      end
    end

  namespace :api, :defaults => {:format => :json} do
    resources :leaguedata
  end

end
