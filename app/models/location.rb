class Location < ActiveRecord::Base
  belongs_to :league
  has_many :games

  acts_as_mappable
  before_validation :geocode_address, on: :create

  private
  def geocode_address
    geo=Geokit::Geocoders::MultiGeocoder.geocode (address)
    errors.add(:address, "Could not Geocode address") if !geo.success
    self.latitude, self.longitude = geo.lat,geo.lng if geo.success
    self.locationUrl = "https://www.google.com/maps?q=#{latitude},#{longitude}"
  end

end
