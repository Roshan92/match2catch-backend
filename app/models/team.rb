class Team < ActiveRecord::Base
  belongs_to :division
  belongs_to :league
  has_many :games
  has_one :standing

  validates :name, :coach, presence: true
end
