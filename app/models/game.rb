class Game < ActiveRecord::Base
  belongs_to :league

  validates :location, :team1_id, :team2_id, :time, presence: true
  validates_exclusion_of :team1, in: :team2, message: "and team2 can't be the same"

end
