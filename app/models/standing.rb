class Standing < ActiveRecord::Base
  belongs_to :league
  belongs_to :team
  # has_and_belongs_to_many :divisions
  belongs_to :division

  validates :teamId, :wins, :losses, presence: true

  validates_uniqueness_of :teamId
end
