class League < ActiveRecord::Base
  has_many :locations
  has_many :games
  has_many :standings
  has_many :divisions
  has_many :teams, through: :divisions

  validates :name, presence: true
end
