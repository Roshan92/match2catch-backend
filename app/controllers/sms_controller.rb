class SmsController < ApplicationController
  before_action :authenticate_user!
  before_action :sms_connection


  def new
    @body_class="skin-blue"
    @all_sms = Sms.all
    @sms = Sms.new
    @balance = @gateway.account.balance
    @new_messages = @gateway.receive
  end

  def create
    require 'textmagic'

    @phone_number = sms_params[:phone_number]
    @sms = sms_params[:sms]

    @sms = Sms.new(sms_params)

    respond_to do |format|
      if @sms.save
        @gateway.send sms_params[:sms], sms_params[:phone_number]
        session[:success] = 'The message to ' + sms_params[:phone_number] + ' containing the text ' + sms_params[:sms] + ' has been sent successfully!'
        format.html { redirect_to new_sm_path }
      else
        session[:fail] = 'Fail to send message. Please check your phone number !'
        format.html { redirect_to new_sm_path }
      end
    end

  end

private

  def sms_connection
    @gateway = TextMagic::API.new('roshan.tamil.sellvan1', 'zwsL80Ptgf')
  end

  def sms_params
    params.require(:sms).permit(:phone_number, :sms)
  end

end