class LeaguesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_league, only: [:show, :edit, :update, :destroy]
  respond_to :html, :json

  # GET /leagues
  # GET /leagues.json
  def index
    @body_class="skin-blue"
    @leagues = League.all
  end

  # GET /leagues/1
  # GET /leagues/1.json
  def show
    @body_class="skin-blue"
  end

  # GET /leagues/new
  def new
    @body_class="skin-blue"
    @league = League.new
    respond_modal_with @league
  end

  # GET /leagues/1/edit
  def edit
    @body_class="skin-blue"
    respond_modal_with @league
  end

  # POST /leagues
  # POST /leagues.json
  def create
    @league = League.new(league_params)

    respond_to do |format|
      if @league.save
        session[:create_league] = 'League was successfully created.'
        format.html { respond_modal_with @league }
        format.json { render :show, status: :created, location: @league }
      else
        session[:create_errors] = @league.errors.full_messages
        format.html { respond_modal_with @league }
        format.json { render json: @league.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /leagues/1
  # PATCH/PUT /leagues/1.json
  def update
    respond_to do |format|
      if @league.update(league_params)
        session[:update_league] = 'League was successfully updated.'
        format.html { respond_modal_with @league }
        format.json { render :show, status: :ok, location: @league }
      else
        session[:update_errors] = @league.errors.full_messages
        format.html { respond_modal_with @league }
        format.json { render json: @league.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /leagues/1
  # DELETE /leagues/1.json
  def destroy
    @league.destroy
    respond_to do |format|
      session[:delete_league] = 'League was successfully destroyed.'
      format.html { redirect_to leagues_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_league
      @league = League.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def league_params
      params.require(:league).permit(:name, :homeScreen, :rulesScreen)
    end
end
