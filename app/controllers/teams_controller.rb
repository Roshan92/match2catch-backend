class TeamsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_division, only: [:index, :create, :new]
  before_action :set_team, only: [:show, :edit, :update, :destroy]
  respond_to :html, :json

  # GET /teams
  # GET /teams.json
  def index
    @body_class="skin-blue"
    @teams = @division.teams
  end

  # GET /teams/1
  # GET /teams/1.json
  def show
    @body_class="skin-blue"
  end

  # GET /teams/new
  def new
    @body_class="skin-blue"
    @team = Team.new
    respond_modal_with @team
  end

  # GET /teams/1/edit
  def edit
    @body_class="skin-blue"
    respond_modal_with @team
  end

  # POST /teams
  # POST /teams.json
  def create
    @team = @division.teams.new(team_params)

    respond_to do |format|
      if @team.save
        session[:create_team] = 'Team was successfully created.'
        format.html { respond_modal_with @team }
        format.json { render :show, status: :created, location: @team }
      else
        session[:create_errors] = @team.errors.full_messages
        format.html { respond_modal_with @team }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /teams/1
  # PATCH/PUT /teams/1.json
  def update
    respond_to do |format|
      if @team.update(team_params)
        session[:update_team] = 'Team was successfully updated.'
        format.html { respond_modal_with @team }
        format.json { render :show, status: :ok, location: @team }
      else
        session[:update_errors] = @team.errors.full_messages
        format.html { respond_modal_with @team }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /teams/1
  # DELETE /teams/1.json
  def destroy
    @team.destroy
    respond_to do |format|
      session[:delete_team] = 'Team was successfully destroyed.'
      format.html { redirect_to division_teams_path(@team.division) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_division
      @division = Division.find(params[:division_id])
    end

    def set_team
      @team = Team.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def team_params
      params.require(:team).permit(:name, :coach, :division_id)
    end
end
