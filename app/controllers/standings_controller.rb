class StandingsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_league, only: [:index, :create, :new]
  before_action :set_standing, only: [:show, :edit, :update, :destroy]
  respond_to :html, :json

  # GET /standings
  # GET /standings.json
  def index
    @body_class="skin-blue"
    @standings = @league.standings
  end

  # GET /standings/1
  # GET /standings/1.json
  def show
    @body_class="skin-blue"
  end

  # GET /standings/new
  def new
    @body_class="skin-blue"
    @standing = Standing.new
    respond_modal_with @standing
  end

  # GET /standings/1/edit
  def edit
    @body_class="skin-blue"
    respond_modal_with @standing
  end

  # POST /standings
  # POST /standings.json
  def create
    #get the team details
    @team = Team.find(standing_params[:teamId])
    teamId = @team.id
    teamName = @team.name
    division = @team.division_id

    #calculation for winning percentage
    @wins = standing_params[:wins]
    @losses = standing_params[:losses]
    winningPct = sprintf "%.3f" % ((@wins.to_f)/((@wins.to_f)+(@losses.to_f)))

    #calculation for pointsDiff
    @pointsFor = (@wins.to_f) * 2
    @pointsAgainst = (@losses.to_f) * 2
    pointsDiff = (@pointsFor.to_i)-(@pointsAgainst.to_i)


    @standing = @league.standings.new(teamId: teamId,
                                      teamName: teamName,
                                      wins: standing_params[:wins],
                                      losses: standing_params[:losses],
                                      winningPct: winningPct,
                                      pointsFor: @pointsFor,
                                      pointsAgainst: @pointsAgainst,
                                      pointsDiff: pointsDiff,
                                      division_id: division,
                                      team_id: teamId)

    respond_to do |format|
      if @standing.save
        session[:create_standing] = 'Standing was successfully created.'
        format.html { respond_modal_with @standing }
        format.json { render :show, status: :created, location: @standing }
      else
        session[:create_errors] = @standing.errors.full_messages
        format.html { respond_modal_with @standing }
        format.json { render json: @standing.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /standings/1
  # PATCH/PUT /standings/1.json
  def update

    #calculation for winning percentage
    @wins = standing_params[:wins]
    @losses = standing_params[:losses]
    winningPct = sprintf "%.3f" % ((@wins.to_f)/((@wins.to_f)+(@losses.to_f)))

    #calculation for pointsDiff
    @pointsFor = (@wins.to_f) * 2
    @pointsAgainst = (@losses.to_f) * 2
    pointsDiff = (@pointsFor.to_i)-(@pointsAgainst.to_i)

    respond_to do |format|
      if @standing.update(
                          wins: standing_params[:wins],
                          losses: standing_params[:losses],
                          winningPct: winningPct,
                          pointsFor: @pointsFor,
                          pointsAgainst: @pointsAgainst,
                          pointsDiff: pointsDiff)

        session[:update_standing] = 'Standing was successfully updated.'
        format.html { respond_modal_with @standing }
        format.json { render :show, status: :ok, location: @standing }
      else
        session[:update_errors] = @standing.errors.full_messages
        format.html { respond_modal_with @standing }
        format.json { render json: @standing.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /standings/1
  # DELETE /standings/1.json
  def destroy
    @standing.destroy
    respond_to do |format|
      session[:destroy_standing] = 'Standing was successfully destroyed.'
      format.html { redirect_to league_standings_path(@standing.league) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_league
      @league = League.find(params[:league_id])
    end

    def set_standing
      @standing = Standing.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def standing_params
      params.require(:standing).permit(:teamId, :teamName, :wins, :losses, :winningPct, :pointsFor, :pointsAgainst, :pointsDiff, :league, :division, :team)
    end
end
