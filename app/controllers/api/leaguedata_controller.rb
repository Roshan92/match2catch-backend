class Api::LeaguedataController < ApplicationController

  def index
    @league = League.all
    respond_to do |format|
      format.json { render :json => @league }
    end
  end

  def show
    @league = League.find(params[:id])
    @division = @league.divisions(@league)
    @location = @league.locations(@league)
    @game = @league.games(@league)

    response = { league: @league.as_json(only: [:name, :id, :homeScreen, :rulesScreen]),
                 teams: @division.as_json(include: { teams: {only: [:id, :name, :coach] } } , only: [:divisionName]),
                 locations: @location.as_json(except: [:created_at, :updated_at, :league_id]),
                 games: @game.as_json(except: [:created_at, :updated_at, :league_id]),
                 standings: @division.as_json(include: {standings: {except: [:id, :created_at, :updated_at, :league_id, :team_id, :division_id] } }, only: [:divisionName]) }

    respond_to do |format|
      format.json { render :json => response }
    end
  end
end
