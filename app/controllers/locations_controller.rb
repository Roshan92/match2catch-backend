class LocationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_league, only: [:index, :create, :new]
  before_action :set_location, only: [:show, :edit, :update, :destroy]
  respond_to :html, :json

  # GET /locations
  # GET /locations.json
  def index
    @body_class="skin-blue"
    @locations = @league.locations
  end

  # GET /locations/1
  # GET /locations/1.json
  def show
    @body_class="skin-blue"
  end

  # GET /locations/new
  def new
    @body_class="skin-blue"
    @location = Location.new
    respond_modal_with @location
  end

  # GET /locations/1/edit
  def edit
    @body_class="skin-blue"
    respond_modal_with @location
  end

  # POST /locations
  # POST /locations.json
  def create
    @location = @league.locations.new(location_params)

    respond_to do |format|
      if @location.save
        session[:create_location] = 'Location was successfully created.'
        format.html { respond_modal_with @location }
        format.json { render :show, status: :created, location: @location }
      else
        session[:create_errors] = @location.errors.full_messages
        format.html { respond_modal_with @location }
        format.json { render json: @location.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /locations/1
  # PATCH/PUT /locations/1.json
  def update
    respond_to do |format|
      if @location.update(location_params)
        session[:update_location] = 'Location was successfully updated.'
        format.html { respond_modal_with @location }
        format.json { render :show, status: :ok, location: @location }
      else
        session[:update_errors] = @location.errors.full_messages
        format.html { respond_modal_with @location }
        format.json { render json: @location.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /locations/1
  # DELETE /locations/1.json
  def destroy
    @location.destroy
    respond_to do |format|
      session[:destroy_location] = 'Location was successfully destroyed.'
      format.html { redirect_to league_locations_path(@location.league) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_league
      @league = League.find(params[:league_id])
    end

    def set_location
      @location = Location.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def location_params
      params.require(:location).permit(:name, :locationUrl, :address, :latitude, :longitude, :league)
    end
end
