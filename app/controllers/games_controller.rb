class GamesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_league, only: [:index, :create, :new]
  before_action :set_game, only: [:show, :edit, :update, :destroy]
  respond_to :html, :json

  # GET /games
  # GET /games.json
  def index
    @body_class="skin-blue"
    @games = @league.games
  end

  # GET /games/1
  # GET /games/1.json
  def show
    @body_class="skin-blue"
  end

  # GET /games/new
  def new
    @body_class="skin-blue"
    @game = Game.new
    respond_modal_with @game
  end

  # GET /games/1/edit
  def edit
    @body_class="skin-blue"
    respond_modal_with @game
  end

  # POST /games
  # POST /games.json
  def create
    #get the location
    @location = Location.find(game_params[:location_id])
    location_id = @location.id
    location = @location.name
    locationUrl = @location.locationUrl

    #get the teams
    @team1_id = Team.find(game_params[:team1_id])
    @team2_id = Team.find(game_params[:team2_id])
    team1_id = @team1_id.id
    team2_id = @team2_id.id
    team1 = @team1_id.name
    team2 = @team2_id.name

    #add new game using all the values above
    @game = @league.games.new(location_id: location_id,
                              location: location,
                              locationUrl: locationUrl,
                              team1_id: team1_id,
                              team1: team1,
                              team2: team2,
                              team2_id: team2_id,
                              team1Score: game_params[:team1Score],
                              team2Score: game_params[:team2Score],
                              time: game_params[:time])

    respond_to do |format|
      if @game.save
        session[:create_game] = 'Game was successfully created.'
        format.html { respond_modal_with @game }
        format.json { render :show, status: :created, location: @game }
      else
        session[:create_errors] = @game.errors.full_messages
        format.html { respond_modal_with @game }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /games/1
  # PATCH/PUT /games/1.json
  def update
    respond_to do |format|
      if @game.update(game_params)
        session[:update_game] = 'Game was successfully updated.'
        format.html { respond_modal_with @game }
        format.json { render :show, status: :ok, location: @game }
      else
        session[:update_errors] = @game.errors.full_messages
        format.html { respond_modal_with @game }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /games/1
  # DELETE /games/1.json
  def destroy
    @game.destroy
    respond_to do |format|
      session[:delete_team] = 'Game was successfully destroyed.'
      format.html { redirect_to league_games_path(@game.league) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_league
      @league = League.find(params[:league_id])
    end

    def set_game
      @game = Game.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def game_params
      params.require(:game).permit(:locationUrl, :location, :location_id, :team1, :team1_id, :team2, :team2_id, :team1Score, :team2Score, :time, :league_id)
    end
end
