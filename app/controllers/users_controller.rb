class UsersController < ApplicationController
  before_filter :authenticate_user!

  def index
  	@body_class="skin-blue"
    @users = User.where.not("id = ?",current_user.id).order("created_at DESC")
    @conversations = Conversation.involving(current_user).order("created_at DESC")
  end
end