class DivisionsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_league, only: [:index, :create, :new]
  before_action :set_division, only: [:show, :edit, :update, :destroy]
  respond_to :html, :json

  # GET /divisions
  # GET /divisions.json
  def index
    @body_class="skin-blue"
    @divisions = @league.divisions
  end

  # GET /divisions/1
  # GET /divisions/1.json
  def show
    @body_class="skin-blue"
  end

  # GET /divisions/new
  def new
    @body_class="skin-blue"
    @division = Division.new
    respond_modal_with @division
  end

  # GET /divisions/1/edit
  def edit
    @body_class="skin-blue"
    respond_modal_with @division
  end

  # POST /divisions
  # POST /divisions.json
  def create
    @division = @league.divisions.new(division_params)

    respond_to do |format|
      if @division.save
        session[:create_division] = 'Division was successfully created.'
        format.html { respond_modal_with @division }
        format.json { render :show, status: :created, location: @division }
      else
        session[:create_errors] = @division.errors.full_messages
        format.html { respond_modal_with @division }
        format.json { render json: @division.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /divisions/1
  # PATCH/PUT /divisions/1.json
  def update
    respond_to do |format|
      if @division.update(division_params)
        session[:update_division] = 'Division was successfully updated.'
        format.html { respond_modal_with @division }
        format.json { render :show, status: :ok, location: @division }
      else
        session[:update_errors] = @division.errors.full_messages
        format.html { respond_modal_with @division }
        format.json { render json: @division.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /divisions/1
  # DELETE /divisions/1.json
  def destroy
    @division.destroy
    respond_to do |format|
      session[:delete_division] = 'Division was successfully destroyed.'
      format.html { redirect_to league_divisions_path(@division.league) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_league
      @league = League.find(params[:league_id])
    end

    def set_division
      @division = Division.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def division_params
      params.require(:division).permit(:divisionName, :league_id)
    end
end
