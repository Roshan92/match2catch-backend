$ ->
  $.rails.allowAction = (link) ->
    return true unless link.attr('data-confirm')
    $.rails.showConfirmDialog(link)
    false

  $.rails.confirmed = (link) ->
    link.removeAttr('data-confirm')
    link.trigger('click.rails')

  $.rails.showConfirmDialog = (link) ->
    message = link.attr 'data-confirm'
    html = """
            <div class="modal" id="confirmationDialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Delete confirmation</h3>
                  </div>
                  <div class="modal-body">
                    <h4>#{message}</h4>
                    <h4>You can't undone this action !</h4>
                  </div>
                  <div class="modal-footer">
                    <a data-dismiss="modal" class="btn btn-default fa fa-times"> Close</a>
                    <a data-dismiss="modal" class="btn btn-danger fa fa-trash-o confirm"> Alright. Just do it !</a>
                  </div>
                </div>
              </div>
            </div>
           """
    $(html).modal()
    $('#confirmationDialog .confirm').on 'click', -> $.rails.confirmed(link)