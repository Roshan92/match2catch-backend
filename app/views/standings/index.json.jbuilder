json.array!(@standings) do |standing|
  json.extract! standing, :id, :teamId, :teamName, :wins, :losses, :winningPct, :pointsFor, :pointsAgainst, :pointsDiff
  json.url standing_url(standing, format: :json)
end
