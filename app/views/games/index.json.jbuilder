json.array!(@games) do |game|
  json.extract! game, :id, :location, :locationUrl, :team1, :team2, :team1Score, :team2Score, :time
  json.url game_url(game, format: :json)
end
