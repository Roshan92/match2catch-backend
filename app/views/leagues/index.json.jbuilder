json.array!(@leagues) do |league|
  json.extract! league, :id, :name, :homeScreen, :rulesScreen
  json.url league_url(league, format: :json)
end
