class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :name
      t.string :locationUrl
      t.text :address

      t.timestamps
    end
  end
end
