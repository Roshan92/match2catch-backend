class CreateDivisions < ActiveRecord::Migration
  def change
    create_table :divisions do |t|
      t.string :divisionName

      t.timestamps
    end
  end
end
