class AddDivisionIdToStanding < ActiveRecord::Migration
  def change
    add_column :standings, :division_id, :integer
  end
end
