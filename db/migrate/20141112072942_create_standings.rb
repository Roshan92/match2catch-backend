class CreateStandings < ActiveRecord::Migration
  def change
    create_table :standings do |t|
      t.integer :teamId
      t.string :teamName
      t.integer :wins
      t.integer :losses
      t.decimal :winningPct
      t.integer :pointsFor
      t.integer :pointsAgainst
      t.integer :pointsDiff

      t.timestamps
    end
  end
end
