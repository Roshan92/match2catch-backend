class AddLeagueIdToStanding < ActiveRecord::Migration
  def change
    add_column :standings, :league_id, :integer
  end
end
