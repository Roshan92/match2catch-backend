class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.text :location
      t.string :locationUrl
      t.integer :locationId
      t.string :team1
      t.integer :team1Id
      t.string :team2
      t.integer :team2Id
      t.integer :team1Score
      t.integer :team2Score
      t.datetime :time

      t.timestamps
    end
  end
end
