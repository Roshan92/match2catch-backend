class Sms < ActiveRecord::Migration
  def change
  	create_table :sms do |t|
      t.string :phone_number
      t.string :sms

      t.timestamps
    end
  end
end
