class RenameColumnInGames < ActiveRecord::Migration
  def change
    rename_column :games, :team1Id, :team1_id
    rename_column :games, :team2Id, :team2_id
  end
end
