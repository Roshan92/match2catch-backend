class AddLeagueIdToDivisions < ActiveRecord::Migration
  def change
    add_column :divisions, :league_id, :integer
  end
end
