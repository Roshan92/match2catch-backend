class AddTeamIdToStandings < ActiveRecord::Migration
  def change
    add_column :standings, :team_id, :integer
  end
end
