class RenameLocationId < ActiveRecord::Migration
  def change
    rename_column :games, :locationId, :location_id
  end
end
