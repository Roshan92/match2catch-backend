League.create([
  {id: 1, name: 'A LEAGUE PEKAKA FOOTBALL'},
  {id: 2, name: 'BUNDESLIGA'},
  {id: 3, name: 'FOOTBALL LEAGUE CHAMPIONSHIP'},
  {id: 4, name: 'EREDIVISIE'},
  {id: 5, name: 'INDIAN LEAGUE'},
  {id: 6, name: 'PEKAKA LEAGUE'}
])

Division.create([
  {id: 1, divisionName: 'Division A', league_id: 1},
  {id: 2, divisionName: 'Division B', league_id: 1},
  {id: 3, divisionName: 'Division A', league_id: 2},
  {id: 4, divisionName: 'Division B', league_id: 2}
])

Team.create([
  {id: 1, name: 'Team Gegulor', coach: 'Abu', division_id: 1},
  {id: 2, name: 'Team Jelutong', coach: 'Ahmad', division_id: 1},
  {id: 3, name: 'Team Ganas', coach: 'Sathia', division_id: 2},
  {id: 4, name: 'Team Terer', coach: 'Lim', division_id: 2}
])

Location.create([
  {id: 1, name: 'Stadium Penang', locationUrl: 'http://goo.gl/qfijVy', address: 'Stadium Penang', latitude: 5.411958, longitude: 100.314041, league_id: 1},
  {id: 2, name: 'Stadium bukit jalil', locationUrl: 'http://goo.gl/vf8P1m', address: 'Stadium bukit jalil', latitude: 3.054428, longitude: 101.690931, league_id: 1},
  {id: 3, name: 'Stadium Shah alam', locationUrl: 'http://goo.gl/esJlmM', address: 'Stadium Shah alam', latitude: 3.082073, longitude: 101.545078, league_id: 2}
])

Game.create([
  {id: 1, location: 'Stadium Penang', locationUrl: 'http://goo.gl/qfijVy', location_id: 1, team1: 'Team Gegulor', team1_id: 1, team2: 'Team Ganas', team2_id: 3, team1Score: 3, team2Score: 2, time: "2015-2-06T18:00:00"},
  {id: 2, location: 'Stadium bukit jalil', locationUrl: 'http://goo.gl/vf8P1m', location_id: 2, team1: 'Team Ganas', team1_id: 3, team2: 'Team Gegulor', team2_id: 1, team1Score: 1, team2Score: 4, time: "2015-2-08T13:00:00"}
])

Standing.create([
  {id: 1, teamId: 1, teamName: 'Team Gegulor', wins: 1, losses: 1, winningPct: '1.000', pointsFor: 50, pointsAgainst: 25, pointsDiff: 25, league_id: 1, division_id: 1},
  {id: 2, teamId: 2, teamName: 'Team Ganas', wins: 1, losses: 1, winningPct: '1.000', pointsFor: 50, pointsAgainst: 25, pointsDiff: 25, league_id: 1, division_id: 2}
])


admin = User.new(
  email: 'icemission@gmail.com',
  first_name: 'Roshan',
  last_name: 'Tamil Sellvan',
  password: 'roselove',
  password_confirmation: 'roselove'
)
admin.skip_confirmation!
admin.save!
